import { BLANK_STATE, GAME_STATE, OVERLAY } from "./states";
import ACTIONS from './action-types';
import { attack } from "./attack";

const DAMAGE_MULTIPLIER = 1;  // Change from 1 for debugging only.

export default function appReducer (state, action) {
    console.log('Fired: ', action);
    switch(action.type) {
        case ACTIONS.START_GAME: 
            return {
                ...BLANK_STATE,
                gameResolution: GAME_STATE.PLAYING,
                overlay: null
            };
        
        case ACTIONS.WIN_GAME:
            return {
                ...BLANK_STATE,
                overlay: OVERLAY.WON
            };
        
        case ACTIONS.LOSE_GAME:
            return {
                ...BLANK_STATE,
                overlay: OVERLAY.LOST
            };
        
        case ACTIONS.ATTACK:
            let [ [h1, h2], [m1, m2] ] = attack();
            let heroHealth = state.gameState.heroHealth;
            let monsterHealth = state.gameState.monsterHealth;
            let gameResolution = state.gameResolution;
            let overlay = state.overlay;

            
            while(h1 + h2 === m1 + m2) {  // Draw is not acceptable.
                [ [h1, h2], [m1, m2] ] = attack();
            }

            if (h1 + h2 < m1 + m2) {
                heroHealth = heroHealth - (m1 + m2 - h1 - h2) * DAMAGE_MULTIPLIER;
                if (heroHealth <= 0) {
                    gameResolution = GAME_STATE.LOST;
                    overlay = OVERLAY.LOST;
                }
            } else {
                monsterHealth = monsterHealth - (h1 + h2 - m1 - m2) * DAMAGE_MULTIPLIER;
                if (monsterHealth <= 0) {
                    gameResolution = GAME_STATE.WON;
                    overlay = OVERLAY.WON;
                }
            }

            return {
                gameResolution,
                gameState: {
                    dice: [ [h1, h2], [m1, m2] ],
                    heroHealth,
                    monsterHealth
                },
                overlay
            };
        
        default:
            throw(new Error('Unknown action!'));
    }
}