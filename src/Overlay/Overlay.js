import './Overlay.scss';
import React from 'react';

export default function Overlay({children}) {
    return (
        <div className="Overlay">
            <div className="Overlay__background"></div>
            <div className="Overlay__content">{ children }</div>
        </div>
    );
}