import appReducer from './app-reducer';
import { BLANK_STATE, GAME_STATE, OVERLAY } from "./states";
import ACTIONS from './action-types';
const attackModule = require('./attack');

beforeEach(() => {
    attackModule.attack = jest.fn();
});

describe('app-reducer', () => {
    describe('inits', () => {
        test('with the intro overlay on', () => {
            expect(
                BLANK_STATE.overlay
            ).toBe(
                OVERLAY.INTRO
            );
        })
    });

    describe('reacts correctly to', () => {
        test('start game', () => {
            expect(
                appReducer({ whateverState: true }, {type: ACTIONS.START_GAME})
            ).toEqual(
                {
                    ...BLANK_STATE,
                    overlay: null,
                    gameResolution: GAME_STATE.PLAYING
                }
            );
        });

        test('winning', () => {
            expect(
                appReducer({ whateverState: true }, { type: ACTIONS.WIN_GAME})
            ).toEqual(
                {
                    ...BLANK_STATE,
                    overlay: OVERLAY.WON
                }
            );
        });

        test('losing', () => {
            expect(
                appReducer({ whateverState: true }, { type: ACTIONS.LOSE_GAME})
            ).toEqual(
                {
                    ...BLANK_STATE,
                    overlay: OVERLAY.LOST
                }
            );
        });

        test('attack on hero', () => {
            const mockDice = [
                [1, 2],
                [4, 6]
            ]
            attackModule.attack.mockReturnValueOnce([
                [1, 2],
                [4, 6]
            ]);
            expect(
                appReducer({
                    gameResolution: GAME_STATE.PLAYING,
                    gameState: {
                        heroHealth: 50,
                        monsterHealth: 40
                    },
                    overlay: null
                }, { type: ACTIONS.ATTACK })
            ).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState:{
                    dice: mockDice,
                    heroHealth: 43,
                    monsterHealth: 40
                },
                overlay: null
            })
        });

        test('attack on monster', () => {
            const mockDice = [
                [4, 4],
                [1, 1]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            expect(
                appReducer({
                    gameResolution: GAME_STATE.PLAYING,
                    gameState: {
                        heroHealth: 50,
                        monsterHealth: 40
                    },
                    overlay: null
                }, { type: ACTIONS.ATTACK })
            ).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState:{
                    dice: mockDice,
                    heroHealth: 50,
                    monsterHealth: 34
                },
                overlay: null
            })
        });

        test('attack to win', () => {
            const mockDice = [
                [4, 4],
                [1, 1]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            expect(
                appReducer({
                    gameResolution: GAME_STATE.PLAYING,
                    gameState: {
                        heroHealth: 50,
                        monsterHealth: 3
                    },
                    overlay: null
                }, { type: ACTIONS.ATTACK })
            ).toEqual({
                gameResolution: GAME_STATE.WON,
                gameState:{
                    dice: mockDice,
                    heroHealth: 50,
                    monsterHealth: -3
                },
                overlay: OVERLAY.WON
            })
        });

        test('attack to lose', () => {
            const mockDice = [
                [2, 1],
                [6, 6]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            expect(
                appReducer({
                    gameResolution: GAME_STATE.PLAYING,
                    gameState: {
                        heroHealth: 9,
                        monsterHealth: 3
                    },
                    overlay: null
                }, { type: ACTIONS.ATTACK })
            ).toEqual({
                gameResolution: GAME_STATE.LOST,
                gameState:{
                    dice: mockDice,
                    heroHealth: 0,
                    monsterHealth: 3
                },
                overlay: OVERLAY.LOST
            })
        });

        test('scenario #1' , () => {
            // Multiple actions. Both start low, and after a few attacks the monster loses, and we reset.
            let mockDice;

            let state = {
                gameResolution: GAME_STATE.PLAYING,
                gameState: {
                    dice: [[1, 2], [3, 4]],
                    heroHealth: 9,
                    monsterHealth: 15
                },
                overlay: null
            };

            // Hero strikes for 6.
            mockDice = [
                [4, 4],
                [1, 1]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            state = appReducer(state, { type: ACTIONS.ATTACK });
            expect(state).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState: {
                    dice: mockDice,
                    heroHealth: 9,
                    monsterHealth: 9
                },
                overlay: null
            });

            // Monster strikes for 5.
            mockDice = [
                [1, 2],
                [2, 6]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            state = appReducer(state, { type: ACTIONS.ATTACK });
            expect(state).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState: {
                    dice: mockDice,
                    heroHealth: 4,
                    monsterHealth: 9
                },
                overlay: null
            });

            // Hero strikes for 10 and wins.
            mockDice = [
                [6, 6],
                [1, 1]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            state = appReducer(state, { type: ACTIONS.ATTACK });
            expect(state).toEqual({
                gameResolution: GAME_STATE.WON,
                gameState: {
                    dice: mockDice,
                    heroHealth: 4,
                    monsterHealth: -1
                },
                overlay: OVERLAY.WON
            });

            // Game is restarted => both full health and no overlay.
            state = appReducer(state, { type: ACTIONS.START_GAME });
            expect(state).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState: {
                    dice: null,
                    heroHealth: 100,
                    monsterHealth: 100
                },
                overlay: null
            });

            // And another strike from the monster to make sure we're good.
            mockDice = [
                [2, 3],
                [2, 4]
            ];
            attackModule.attack.mockReturnValueOnce(mockDice);
            state = appReducer(state, { type: ACTIONS.ATTACK });
            expect(state).toEqual({
                gameResolution: GAME_STATE.PLAYING,
                gameState: {
                    dice: mockDice,
                    heroHealth: 99,
                    monsterHealth: 100
                },
                overlay: null
            });
        });

        test('unknown action', () => {
            expect(
                () => appReducer(BLANK_STATE, { type: 'UNKNOWN!'})
            ).toThrow();
        });
    });
});