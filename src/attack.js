export const diceRoll = () => Math.ceil( Math.random() * 6 );

export const attack = () => [
    [ diceRoll(), diceRoll() ],  // Hero
    [ diceRoll(), diceRoll() ]   // Enemy
];