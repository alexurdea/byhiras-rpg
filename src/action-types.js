const ACTIONS = {
    START_GAME: 'START GAME',
    WIN_GAME: 'WIN',
    LOSE_GAME: 'LOSE',
    ATTACK: 'ATTACK'
};

export default ACTIONS;