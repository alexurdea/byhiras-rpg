import React from 'react';
import './HealthBar.css';

export default function HealthBar({health}) {
    return (
        <div className="meter animate">
            <span style={{width: health + '%'}}></span>
        </div>
    );
}