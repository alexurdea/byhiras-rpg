import React from 'react';
import readyImg from '../assets/Are-you-ready-for-advent.png';
import startImg from '../assets/Start.png';
import skullImg from '../assets/skull3.png';
import './Loss.scss';

export default function Intro ({start}) {
    return (
        <div>
            <p>Oh, no... You lost. But you're not the type that gives up so easily, are you? Try again!</p>
            <img className="skull" src={skullImg} />
            <img className="start" src={startImg} onClick={start} />
        </div>
    );
}