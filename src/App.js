import React, {useReducer} from 'react';
import './App.scss';
import sceneBgImg from './assets/scene-bg.png';
import titleImg from './assets/battle-generator-title.png';
import heroImg from './assets/knight.png';
import ghostImg from './assets/ghost.png';
import attackImg from './assets/attack.png';
import d1Img from './assets/dice1.png';
import d2Img from './assets/dice2.png';
import d3Img from './assets/dice3.png';
import d4Img from './assets/dice4.png';
import d5Img from './assets/dice5.png';
import d6Img from './assets/dice6.png';
import HealthBar from './HealthBar/HealthBar';
import appReducer from './app-reducer';
import { BLANK_STATE, GAME_STATE, OVERLAY } from "./states";
import ACTIONS from './action-types';
import Overlay from './Overlay/Overlay';
import Intro from './Intro/Intro';
import Loss from './Loss/Loss';
import Victory from './Victory/Victory';

function App() {
  const [state, dispatch] = useReducer(appReducer, BLANK_STATE);

  console.log(state);

  const dice = state.gameState.dice ? (
    <div className="App__dice">
      <img className={'Dice__d d' + state.gameState.dice[0][0]} />
      <img className={'Dice__d d' + state.gameState.dice[1][0]} />
      <span className="flex-break"></span>
      <img className={'Dice__d d' + state.gameState.dice[0][1]} />
      <img className={'Dice__d d' + state.gameState.dice[1][1]} />
    </div>
  ) : <div className="App__dice"></div>;
  const overlayContent = state.overlay === OVERLAY.INTRO ? <Intro start={() => dispatch({ type: ACTIONS.START_GAME })} /> :
    state.overlay === OVERLAY.LOST ? <Loss start={() => dispatch({ type: ACTIONS.START_GAME })} /> :
    state.overlay === OVERLAY.WON ? <Victory start={() => dispatch({ type: ACTIONS.START_GAME })} />:
    null;

  return (
    <div className="App">
      
      <div className="App__scene">
        { state.overlay ? <Overlay>{ overlayContent }</Overlay> : null }
        
        <div className="App__header"></div>

        <div className="App__battle">
          <div className="App__hero character">
            <img className="character__pic" src={heroImg}/>
            <HealthBar health={state.gameState.heroHealth} />
          </div>

          { dice }
          
          <div className="App__enemy character">
            <img className="character__pic" src={ghostImg} />
            <HealthBar health={state.gameState.monsterHealth} />
          </div>
        </div>

        <div className="App__attack">
          {
            state.gameResolution === GAME_STATE.PLAYING ?
            <img className="App__attack-img--disabled" src={attackImg} onClick={() => dispatch({ type: ACTIONS.ATTACK })} />:
            null
          }
        </div>
      </div>
    </div>
  );
}

export default App;
