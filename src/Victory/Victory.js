import React from 'react';
import readyImg from '../assets/Are-you-ready-for-advent.png';
import startImg from '../assets/Start.png';
import trophyImg from '../assets/trophy.png';
import './Victory.scss';

export default function Intro ({start}) {
    return (
        <div>
            <p>You win! Play again?</p>
            <img className="skull" src={trophyImg} />
            <img className="start" src={startImg} onClick={start} />
        </div>
    );
}