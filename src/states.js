export const GAME_STATE = { 
    PLAYING: 'PLAYING', 
    WON: 'WON', 
    LOST: 'LOST'
};
  
export const OVERLAY = { 
    INTRO: 'INTRO', 
    WON: 'WON', 
    LOST: 'LOST'
};
  
export const BLANK_STATE = {
    gameResolution: null,
    gameState: {
        dice: null,
        heroHealth: 100,
        monsterHealth: 100
    },
    overlay: OVERLAY.INTRO
};