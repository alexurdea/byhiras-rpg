# Run
The app is based on create-react-app, so all the specific npm tasks are available.

# Deployment
The game is deployed on AWS (CloudFront + S3): http://byhiras-rpg-au.s3-website.eu-west-2.amazonaws.com/
It has been tested and works well in both Chrome and Edge.

# Architecture
I decided to go with useReducer, as Redux seemed overkill, given that the component tree is shallow and simple.
I usually go with TypeScript, but due to the time constraints, I picked JS for this task.

# Testing
The approach was TDD. The most important and most complex piece of logic is the app reducer. It includes a full battle scenario test, 
beside the basic unit tests.

I usually cover everything with tests, but took some shortcuts due to time constraints.


# Layout
The layout is adaptable, up to a point. The stage needs a minimum width and height.
