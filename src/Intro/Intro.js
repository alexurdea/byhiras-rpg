import React from 'react';
import readyImg from '../assets/Are-you-ready-for-advent.png';
import startImg from '../assets/Start.png';
import './Intro.scss';

export default function Intro ({start}) {
    return (
        <div>
            <img className="Intro__ready" src={readyImg} />
            <img className="start" src={startImg} onClick={start} />
        </div>
    );
}